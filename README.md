# mercari-web-tests

This Cypress based sample tests project is implemented with the help of TypeScript and uses page-object model as the design pattern with BDD approach.

**Structure:**

```

├── cypress
│   └── specs (feature files)
|   └── step-definitions (tests code)
│   └── ui-identifiers (page objects)
└── cypress.json (cypress global configuration)

```
   
**Features:**

- Has the capability to run on various viewports such as desktop, tablet and mobile.
- Based on Cucumber / Gherkin standard.
- Cross-browser platform.

**Installation**:

- Clone the project
- `npm install`
- `npm run cy:open` (headed mode) or
- `npm run cy:run` (headless mode)