Feature: Search Item

As a user, I want to search for an item of my choice so that I can view the product details and proceed ahead with shopping.
    
    Scenario Outline: Search for an item
        Given User visits '/' on '<viewport>' device
        When User search for an item 'MacBook'
        Then User navigates to search results page
        And User sees all the results realted to 'MacBook'
    
    Examples:
        |viewport|
        |desktop|
        |tablet|
        |mobile|
    
    Scenario Outline: View an item after search
        Given User visits '/' on '<viewport>' device
        And User search for an item 'MacBook'
        When User selects the '3rd' item from given search results
        Then User navigates to item details page
        And User verifies the item title to include 'MacBook'
    
    Examples:
        |viewport|
        |desktop|
        |tablet|
        |mobile|