Feature: Shipping Address

As a user, I want to add a new shipping address to my account so that the product of my choice gets delivered to me at my doorstep.

    Background: Navigate to shipping address page after successful authentication
        * User visits '/' on 'desktop' device
        * User authenticate himself by entering username as 'nabil@gmail.com' and password as 'test@123'
        * User sees Home page
        * User hover on My Page and chooses Personal information
        * User clicks on Shipping Address

    Scenario: Add new shipping address
        Given User is on 'Address list' page
        And User sees 'Register new address' button 
        When User clicks on 'Register new address' button
        Then User naviagtes to 'Address registration' page where user sees a form to fill
        When User populates all the mandatory fields by providing full name as 'Nabil Shaikh'
        And User clicks on Save button
        Then User should land back to Address list page
        And User sees a growl message as 'Address saved!'
        And User verifies newly added shipping address is present with a name 'Nabil Shaikh'
    
    Scenario: Verify newly added shipping address
        Given User is on 'Address list' page
        And User verifies newly added shipping address is present with a name "Nabil Shaikh"
        And User sees Edit and Delete buttons besides the address
        Then User sees 'Register new address' button to add another address

    Scenario: Delete a shipping address
        Given User is on 'Address list' page
        When User clicks on Delete button for an address which has a name 'Nabil Shaikh'
        Then User sees a confirmation popup asking to confirm deletion process
        When User clicks on Confirm button 
        Then User verifies shipping address with a name 'Nabil Shaikh' is deleted successfully