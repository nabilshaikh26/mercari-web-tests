/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Login {
  getUsernameTextField() {
    return cy.get('[data-test=username]');
  }

  getPasswordTextField() {
    return cy.get('[data-test=password]');
  }

  getLoginButton() {
    return cy.get('[data-test=LogIn]');
  }
}

export default Login;
