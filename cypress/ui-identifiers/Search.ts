/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Search {
  getResultsHeader() {
    return cy.get('.search-result-head');
  }

  getAllItems() {
    return cy.get('.items-box');
  }
}

export default Search;
