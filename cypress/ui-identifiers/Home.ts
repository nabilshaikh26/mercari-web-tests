/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Home {
  getSearchBox() {
    return cy.get('[type=search]').then((element) => (Cypress.dom.isVisible(element.first()) ? element.first() : element.last()));
  }

  getMyPageTab() {
    return cy.get('[data-test=mypage]');
  }

  getPersonalInformationHyperlink() {
    return cy.get('[data-test=personal-information]');
  }
}

export default Home;
