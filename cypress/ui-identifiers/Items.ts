/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class Items {
  getItemName() {
    return cy.get('.item-name');
  }
}

export default Items;
