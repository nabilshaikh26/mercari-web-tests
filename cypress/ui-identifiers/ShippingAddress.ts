/* eslint-disable class-methods-use-this */
/// <reference types="cypress" />

class ShippingAddress {
  getShippingAddressTab() {
    return cy.get('[data-test=shipping_address]');
  }

  getShippingAddressRegisterButton() {
    return cy.get('[data-test=register_address]');
  }

  getFullNameTextField() {
    return cy.get('[data-test=fullname]');
  }

  getAddressTextField() {
    return cy.get('[data-test=address]');
  }

  getZipcodeTextField() {
    return cy.get('[data-test=zipcode]');
  }

  getSaveButton() {
    return cy.get('[data-test=save-button]');
  }

  getGrowlMessageBox() {
    return cy.get('[data-test=growl-message]');
  }

  getShippingAddressCard() {
    return cy.get('[data-test=address-row]');
  }

  getShippingAddressEditButton() {
    return cy.get('[data-test=address-edit-button]');
  }

  getShippingAddressDeleteButton() {
    return cy.get('[data-test=address-delete-button]');
  }

  getDeleteAddressAlert() {
    return cy.get('[data-test=delete-alert]');
  }

  getAlertConfirmButton() {
    return cy.get('[data-test=delete-alert-confirm]');
  }
}

export default ShippingAddress;
