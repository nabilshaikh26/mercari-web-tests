/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="cypress" />

import { And, Then, When } from 'cypress-cucumber-preprocessor/steps';
import Home from '../ui-identifiers/Home';
import Items from '../ui-identifiers/Items';
import Search from '../ui-identifiers/Search';

const home = new Home();
const search = new Search();
const items = new Items();

When('User search for an item {string}', (searchString) => {
  home.getSearchBox().click().type(searchString).type('{enter}');
});

Then('User navigates to search results page', () => {
  cy.url().then((urlString) => {
    expect(urlString).to.include('/search/?keyword');
  });
});

And('User sees all the results realted to {string}', (searchString) => {
  search.getResultsHeader().then((header) => {
    expect(header.text().trim()).to.contains('の検索結果', searchString);
  });
});

When('User selects the {string} item from given search results', (itemIndex) => {
  search.getAllItems().eq(parseInt(itemIndex) - 1).click();
});

Then('User navigates to item details page', () => {
  cy.url().then((urlString) => {
    expect(urlString).to.include('/items/');
  });
});

And('User verifies the item title to include {string}', (title) => {
  items.getItemName().then((itemName) => {
    expect(itemName.text().trim()).to.include(title);
  });
});
