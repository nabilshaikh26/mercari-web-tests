/* eslint-disable import/no-extraneous-dependencies */
/// <reference types="cypress" />

import {
  And, Given, Then, When,
} from 'cypress-cucumber-preprocessor/steps';
import Home from '../ui-identifiers/Home';
import Login from '../ui-identifiers/Login';
import ShippingAddress from '../ui-identifiers/ShippingAddress';

const home = new Home();
const login = new Login();
const address = new ShippingAddress();

And('User authenticate himself by entering username as {string} and password as {string}', (username, password) => {
  login.getUsernameTextField().click().type(username);
  login.getPasswordTextField().click().type(password);
  login.getLoginButton().click();
});

And('User sees Home page', () => {
  cy.url().then((urlString) => {
    expect(urlString).to.include('/home');
  });
});

And('User hover on My Page and chooses Personal information', () => {
  home.getMyPageTab().trigger('mouseover');
  home.getPersonalInformationHyperlink().click();
});

And('User clicks on Shipping Address', () => {
  address.getShippingAddressTab().click();
});

// Scenario 1

Given("User is on 'Address list' page", () => {
  cy.url().then((urlString) => {
    expect(urlString).to.include('/addressList');
  });
});

And("User sees 'Register new address' button", () => {
  address.getShippingAddressRegisterButton().should('be.visible');
});

When("User clicks on 'Register new address' button", () => {
  address.getShippingAddressRegisterButton().click();
});

Then("User naviagtes to 'Address registration' page where user sees a form to fill", () => {
  cy.url().then((urlString) => {
    expect(urlString).to.include('/addressRegistration');
  });
});

When('User populates all the mandatory fields by providing full name as {string}', (fullNameString) => {
  address.getFullNameTextField().click().type(fullNameString);
  address.getAddressTextField().click().type('206 Sunshine Apartment');
  address.getZipcodeTextField().click().type('400008');
});

And('User clicks on Save button', () => {
  address.getSaveButton().click();
});

Then('User should land back to Address list page', () => {
  cy.url().then((urlString) => {
    expect(urlString).to.include('/addressList');
  });
});

And('User sees a growl message as {string}', (messageText) => {
  address.getGrowlMessageBox().then((header) => {
    expect(header.text().trim()).to.contain(messageText);
  });
});

And('User verifies newly added shipping address is present with a name {string}', (fullNameString) => {
  address.getShippingAddressCard().eq(0).should('contain', fullNameString);
});

// Scenario 2

And('User sees Edit and Delete buttons besides the address', () => {
  address.getShippingAddressCard().eq(0).then(() => {
    address.getShippingAddressEditButton().should('be.visible');
    address.getShippingAddressDeleteButton().should('be.visible');
  });
});

Then("User sees 'Register new address' button to add another address", () => {
  address.getShippingAddressRegisterButton().should('be.visible');
});

// Scenario 3

When('User clicks on Delete button for an address which has a name {string}', (fullNameString) => {
  address.getShippingAddressCard().eq(0).should('have.text', fullNameString);
  address.getShippingAddressCard().eq(0).then(() => {
    address.getShippingAddressDeleteButton().click();
  });
});

Then('User sees a confirmation popup asking to confirm deletion process', () => {
  address.getDeleteAddressAlert().should('be.visible');
});

When('User clicks on Confirm button', () => {
  address.getAlertConfirmButton().click();
});

Then("User verifies shipping address with a name 'Nabil Shaikh' is deleted successfully", () => {
  address.getShippingAddressCard().should('not.exist');
});
